#

/*
 * Copyright c. John Hallam <sw@j.hallam.dk> 2015.
 *
 * This program is free software licensed under the terms of the GNU General
 * Public License, either version 3 of the License, or (at your option) any
 * later version.  See http://www.gnu.org/licenses/gpl.txt for details.
 */

#include "general.h"

/*
 * The ZMQ addresses for the reader thread
 */

#define READER_CMD_ADDR "inproc://Reader-CMD"
#define READER_QUEUE_ADDR "inproc://Reader-Q"

/*
 * READER parameter structure.
 *
 * The order of r_range and r_bufsz seems to alter whether the
 * parameters are correctly initialised or not by the param code...
 */

typedef struct {
  int         r_schedprio;         /* Reader real-time priority */
  double      r_frequency;         /* Per-channel sampling frequency [Hz] */
  int         r_range;             /* ADC full-scale range [mV] */
  int         r_bufsz;             /* Reader buffer size [MiB] */
  int	      r_bps;		   /* Bits per sample from ADC */
  double      r_window;            /* Snapshot window [s] (must fit in buffer) */
  double      r_buf_hwm_fraction;  /* Ring buffer high-water mark as fraction of size */
  double      r_sync_wait_time;	   /* Time to wait for first data after capture start */
  double      r_sscorrelation;	   /* Correlation coefficient between successive (channel) samples */
  const char *r_device;            /* Comedi device to use */
  int         r_running;           /* Thread is running and ready */
}
  rparams;

export rparams   reader_parameters;

/*
 * READER clock mapping information.
 *
 * This records the current sample numbers associated with the latest
 * monotonic clock timestamp in the main READER loop.  This is set
 * during the loop when sufficient time has passed.  Note that it is
 * volatile (with respect to other threads) since the READER may
 * change the data at any time its main loop cycles.
 *
 * Thread-safe export of this data is handled by the reader_get_clockmap() routine.
 */

typedef struct {
  uint64_t    rcm_head;		   /* The head sample in the buffer at a given time */
  uint64_t    rcm_tail;		   /* The tail sample in the buffer at a given time */
  uint64_t    rcm_nsclock;	   /* The given time, in nanoseconds since the UNIX epoch */
  const char *rcm_state;	   /* The READER state machine state */
  int	      rcm_adc_live;	   /* The READER ADC liveness state */
  int	      rcm_adc_nchan;       /* Number of channels in the output data */
  int	      rcm_adc_isp;	   /* Time between successive samples (adjacent channels) [ns] */
} rclockmap;

export int	 reader_get_clockmap(rclockmap *);

export int       verify_reader_params(rparams *, strbuf);
export void     *reader_main(void *);

