#

/*
 * Copyright c. John Hallam <sw@j.hallam.dk> 2015.
 *
 * This program is free software licensed under the terms of the GNU General
 * Public License, either version 3 of the License, or (at your option) any
 * later version.  See http://www.gnu.org/licenses/gpl.txt for details.
 */

#include "general.h"

#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <comedilib.h>

#include "error.h"
#include "assert.h"
#include "util.h"
#include "queue.h"
#include "mman.h"
#include "strbuf.h"
#include "chunk.h"

/* Structure for a memory block descriptor */

typedef struct {
  void *b_data;
  int   b_bytes;
}
  block;

/*
 * Information about a memory frame: the block of memory (length and
 * origin), a file descriptor and an active flag.
 *
 * If the file descriptor is -1, the frame is released by unmapping
 * it; if not, by writing the data to the specified file descriptor.
 * This needs to happen in a thread that can wait.
 */

#define FRAME_EMPTY	0
#define FRAME_ANON	1
#define FRAME_MAPPED	2

#define FRAME_DEAD	0
#define FRAME_MORBID	1
#define FRAME_LIVE	3

struct _frame {
  queue f_Q;
  block f_map;
  int   f_state;		/* LIVE, MORBID, DEAD */
  int	f_mapping;		/* MAPPED, ANON, EMPTY */
  int   f_fd;
};

#define qp2frame(p)	((frame *)(p))
#define frame2qp(f)	(&((f)->f_Q))

/*
 * Set up the mmap frames for data transfer to snapshot files.
 */

private int    nframes;    /* The number of simultaneous mmap frames */
private frame *framelist;  /* The list of mmap frame descriptors */
private int    n_frame_Q = 0;

private QUEUE_HEADER(frameQ);

/*
 * Initialise the frame system
 *
 * Set up nfr frames each of size chunk [kib], using in total ram [MiB] of memory.
 */

public int init_frame_system(strbuf e, int nfr, int ram, int chunk) {
  int bsize = chunk*1024;
  
  LOG(WRITER, 1, "Init %d frames of size %d[kiB] in ram %d[MiB]\n", nfr, chunk, ram);

  framelist = (frame *)calloc(nfr, sizeof(frame));
  if( framelist ) {
    void *map = mmap_locate(ram*1024*1024, 0);
    int   n;

    if(map == NULL) {
      strbuf_appendf(e, "Cannot mmap %d[MiB] of locked transfer RAM: %m", ram);
      free((void *) framelist );
      return -1;
    }
    munmap(map, ram*1024*1024);
    
    for(n=0; n<nfr; n++, map+=bsize) { /* Initialise the frame memory pointers, leave sizes as 0 */
      init_queue(&framelist[n].f_Q);
      queue_ins_before(&frameQ, frame2qp(&framelist[n]));
      framelist[n].f_map.b_data  = map;
      framelist[n].f_map.b_bytes = bsize;
      framelist[n].f_state   = FRAME_DEAD;
      framelist[n].f_mapping = FRAME_EMPTY;
      framelist[n].f_fd = -1;
      n_frame_Q++;
    }
  }
  else {
    strbuf_appendf(e, "Cannot allocate frame list memory for %d frames: %m", nfr);
    return -1;
  }
  nframes = nfr;
  return 0;
}

/*
 * Allocate a frame descriptor.
 */

private frame *alloc_frame() {
  frame *f;

  if( !n_frame_Q ) {
    errno = EBUSY;
    return NULL;
  }
  errno = 0;
  f = qp2frame( de_queue(queue_next(&frameQ)) );
  assertv(f != NULL, "Frame queue count %d but queue is empty\n", n_frame_Q);
  n_frame_Q--;
  f->f_state = FRAME_LIVE;         /* In-use;  mark as active */
  return f;
}

/*
 * Free a frame descriptor
 *
 * The mapping state of the descriptor is ANON or EMPTY
 *
 * This code is called in the WRITER as part of unmapping a chunk,
 * since the frames are allocated in the WRITER during chunk mapping.
 */

public void free_frame(frame *f) {
  f->f_state = FRAME_DEAD;
  queue_ins_before(&frameQ, frame2qp(f));
  n_frame_Q++;
}

/*
 * If a MAPPED frame arrives here its chunk had an error.  The file
 * is deleted so the memory is now ANON.  The frame becomes MORBID.
 *
 * This code is used in the READER when a chunk has an error and its
 * frame doesn't need to be released by TIDY.
 */

public void abandon_frame(frame *f) {
 if(f->f_mapping == FRAME_MAPPED) {
    f->f_mapping = FRAME_ANON;
  }
 f->f_state = FRAME_MORBID;
}

/*
 * Report the index of a frame pointer in the table.
 */

public int frame_nr(frame *f) {
  return f - framelist;
}

/*
 * Dump a frame structure, for debugging.
 */

public void dump_frame(frame *f, char *buf, size_t n) {
  snprintf(buf, n, "Frame %d: state: %d map: %d fd: %d block: { %p %d }",
	   frame_nr(f), f->f_state, f->f_mapping, f->f_fd,
	   f->f_map.b_data, f->f_map.b_bytes
	   );
}

/*
 * Release a frame descriptor.  
 *
 * This is done in the TIDY thread, since it might block.
 *
 * A file-mode snapshot comprises chunks with MAPPED frames; they are
 * unmapped using munmap and are then EMPTY.
 *
 * A stream-mode snapshot comprises chunks with ANON frames, which are
 * written.  The memory doesn't need to be freed.
 */

public int release_frame(frame *f) {
  assertv(f->f_map.b_data != NULL, "Frame %d doesn't have a valid memory block descriptor\n", frame_nr(f));

  f->f_state = FRAME_MORBID;
  errno = 0;

  switch(f->f_mapping) {
  case FRAME_MAPPED:
    munmap(f->f_map.b_data, f->f_map.b_bytes);
    f->f_mapping = FRAME_EMPTY;
    break;
  
  case FRAME_ANON: 
    assertv(f->f_fd >= 0, "Frame %d is ANON with invalid file descriptor %d\n", frame_nr(f), f->f_fd);
    write(f->f_fd, f->f_map.b_data, f->f_map.b_bytes); /* Sets errno if it fails */
    break;

  default:
    assertv(0, "Frame %d is empty so cannot be processed by release_frame\n", frame_nr(f));
    /* NOT REACHED */
  }
  return errno;
}

/*
 * Functions for dealing with transfer chunk descriptors.
 */

private uint16_t chunk_counter;

#define N_CHUNK_ALLOC   (4096/sizeof(chunk_t))

private QUEUE_HEADER(chunkQ);
private int N_in_chunkQ = 0;

/*
 * Allocate n new chunk descriptors, chained using the WRITER queue descriptor
 */

public chunk_t *alloc_chunk(int nr) {
  queue *ret;

  if( N_in_chunkQ < nr ) {      /* The queue doesn't have enough */
    int n;

    for(n=0; n<N_CHUNK_ALLOC; n++) {
      queue *q = (queue *)calloc(1, sizeof(chunk_t));

      if( !q ) {                        /* Allocation failed */
        if( N_in_chunkQ >= nr )
          break;                        /* But we have enough now anyway */
        return NULL;
      }
      init_queue(q);
      queue_ins_after(&chunkQ, q);
      N_in_chunkQ++;
    }
  }

  ret = de_queue(queue_next(&chunkQ));
  N_in_chunkQ--;
  chunk_t *c = qp2chunk(ret);
  init_queue(&c->c_rQ);
  c->c_name = ++chunk_counter;
  
  while(--nr > 0) {             /* Collect enough to satisfy request */
    chunk_t *c = qp2chunk(de_queue(queue_next(&chunkQ)));

    init_queue(&c->c_wQ);       /* Redundant... */
    init_queue(&c->c_rQ);
    c->c_name = ++chunk_counter;
    queue_ins_before(ret, chunk2qp(c));
    N_in_chunkQ--;
  }

  return c;
}

/*
 * Finished with chunk descriptors chained using the writer queue descriptor.
 * Assume the reader queue descriptor is detached.
 */

public void release_chunk(chunk_t *c) {
  queue *q = chunk2qp(c);
  queue *p;

  while( (p = de_queue(queue_next(q))) != NULL ) {
    init_queue(p);
    queue_ins_before(&chunkQ, p);
    N_in_chunkQ++;
  }
  init_queue(q);
  queue_ins_before(&chunkQ, q);
  N_in_chunkQ++;
}

/*
 * Find a frame for a chunk and map the chunk into memory.  This may
 * take arbitrary time since this is where Linux has to find us new
 * pages.  This code runs in the WRITER thread.
 */

public int chunk_add_frame(chunk_t *c) {
  frame *fp = alloc_frame();
  void  *map;

  if(fp == NULL) {   /* This is not a fatal error:  there may be no frames available for transient reasons */
    return -1;
  }

  if(fp->f_map.b_bytes != c->c_samples*sizeof(sampl_t)) { /* The frame is the wrong size; mark it EMPTY */
    fp->f_map.b_bytes = c->c_samples*sizeof(sampl_t);
    fp->f_mapping = FRAME_EMPTY;
  }
  
  /*
   * If the chunk is a stream component, set the frame fd to the chunk fd, otherwise to -1
   */

  errno = 0;
  if(chunk_of_stream(c)) {	/* Writing to stream -- use anonymous pages, re-use if possible */
    if(fp->f_mapping == FRAME_EMPTY) {	/* Frame is EMPTY, so find new ANON pages for it */
      map = mmap_and_lock(-1, 0, fp->f_map.b_bytes, PROT_RDWR|PREFAULT_RDWR|MAL_LOCKED);
      fp->f_map.b_data = map;
      fp->f_mapping = FRAME_ANON;
    }
    fp->f_fd = c->c_fd;
  } else {			/* Writing to file -- use file-mapped pages */
    map = mmap_and_lock(c->c_fd, c->c_offset, fp->f_map.b_bytes, PROT_RDWR|PREFAULT_RDWR|MAL_LOCKED);
    fp->f_map.b_data = map;
    fp->f_fd = -1;
    fp->f_mapping = FRAME_MAPPED;
  }
  LOG(WRITER, 3, "Map chunk %s in frame %d from fd %d offs %d with addr %p and size %d[B] gives res %p\n",
      c_nstr(c), frame_nr(fp), c->c_fd, c->c_offset, fp->f_map.b_data, fp->f_map.b_bytes, map);

  /*
   * The c_error strbuf is available because this runs in the WRITER thread.
   */

  if(map == NULL) {
    strbuf_appendf(c->c_error, "Unable to map chunk %s to frame %d: %m", c_nstr(c), frame_nr(fp));
    set_chunk_status(c, SNAPSHOT_ERROR);
    fp->f_mapping = FRAME_EMPTY;
    free_frame(fp);		/* Frame was allocated, so need to free it here */
    return -1;
  }
  
  fp->f_state = FRAME_LIVE;
  c->c_frame = fp;              /* Succeeded, chunk now has a mapped frame */
  return 0;
}

/*
 * Copy the data for a chunk from the ring buffer into the frame.  Apply the
 * appropriate ADC conversion.  Used by the READER thread.
 *
 * This routine assumes that when the chunk's ring buffer pointer is computed by
 * adc_setup_chunk(), it also ensures that the previous sample to the chunk's
 * first sample is available in the ring buffer at the previous address (the
 * sample value is needed for decorrelation processing in the LUT module).  Note
 * the need to handle the edge case where the chunk starts at sample 0.
 */

public void copy_chunk_data(chunk_t *c) {
  convertfn fn = c->c_convert;
  sampl_t   prev;

   LOG(READER, 3, "Copy chunk %s using fn %p from %p to %p size %d[spl]\n", 
       c_nstr(c), fn, c->c_ring, c->c_frame->f_map.b_data, c->c_samples);

   prev = c->c_first > 0? ((sampl_t *)c->c_ring)[-1] : 0;
   (*fn)((sampl_t *)c->c_frame->f_map.b_data, (sampl_t *)c->c_ring, c->c_samples, prev);
   set_chunk_status(c, SNAPSHOT_WRITTEN);
}

/*
 * If an error occurs when the chunk is owned by the READER, it's
 * report cannot be written to the error strbuf.  This must be done
 * when the chunk returns to the WRITER.  The error code is stored in
 * the chunk c_errno field in the meantime and here it is written
 * across to the error strbuf.
 *
 * Error codes repurposed for this application are caught here:
 *
 * ENOSR:	   no data available because ADC not ARMED or RUNNING
 * EADDRNOTAVAIL:  the request arrived after the data left the ring buffer
 * ENODATA:	   the ADC data source has dried up
 * ECANCELED:	   a halt command has stopped the ADC data stream
 */

public void propagate_chunk_errno(chunk_t *c) {
  if(chunk_saw_error(c)) {
    char *fmt;
    
    switch(c->c_errno) {

    default:
      fmt = "Chunk %s async error: %m";
      errno = c->c_errno;
      break;

    case ECONNABORTED:
      fmt = "Chunk %s async error: ADC error in Comedi\n";
      break;

    case ENOSR:
      fmt = "Chunk %s async error: ADC is not running\n";
      break;

    case EADDRNOTAVAIL:
      fmt = "Chunk %s async error: data requested too late\n";
      break;

    case ENODATA:
      fmt = "Chunk %s async error: ADC dataflow dried up\n";
      break;

    case ECANCELED:
      fmt = "Chunk %s async error: ADC halted by command\n";

    case 0:			/* No error */
      return;
    }

    strbuf_appendf(c->c_error, fmt, c_nstr(c));
    c->c_errno = 0;
  }
}

/*
 * Generate a debugging line for a chunk desdcriptor.  Put it in the buffer buf.
 * Return the actual size, no greater than the space available.
 */

#define qp2cname(p)     (c_nstr(qp2chunk(p)))

public const char *rq2cname(queue *p) {
  import queue *rcQp, *wcQp;

  if(p == rcQp)
    return "RQhead";
  if(p == wcQp)
    return "WQhead";
  return c_nstr(rq2chunk(p));
}

public int debug_chunk(char buf[], int space, chunk_t *c) {
  import const char *snapshot_status(int);
  import const char *snapfile_name(snapfile_t *);
  int used;

  used = snprintf(buf, space,
                  "chunk %s at %p "
                  "wQ[%s,%s] "
                  "rQ[%s,%s] "
                  "RG %p FR %p PF %s status %s "
                  "S:%08lx F:%016llx L:%016llx "
		  "(NFQ %3d NCQ %3d)\n",
                  c_nstr(c), c,
                  qp2cname(queue_prev(&c->c_wQ)), qp2cname(queue_next(&c->c_wQ)),
                  rq2cname(queue_prev(&c->c_rQ)), rq2cname(queue_next(&c->c_rQ)),
                  c->c_ring, c->c_frame, snapfile_name(c->c_parent), snapshot_status(c->c_status),
                  c->c_samples, c->c_first, c->c_last,
		  n_frame_Q, N_in_chunkQ
                  );
  if(used >= space)
    used = space;
  return used;
}

/*
 * Routines to handle status codes for chunks, snapfiles and snapshots.
 *
 * Display snapshot status codes
 * Return chunk owner
 * Set chunk owner
 */

public const char *snapshot_status(int st) {
  private const char *stabf[]
    = {
    " INI", " ERR", " PRP", " RDY", " ...", " >>>", " +++", " FIN",
  };
  private const char *stabs[]
    = {
    "|INI", "|ERR", "|PRP", "|RDY", "|...", "|>>>", "|+++", "|FIN",
  };
  const char **stab = is_stream(st) ? stabs : stabf;

  st &= SNAPSHOT_STATUS_MASK;
  if(st>=0 && st<sizeof(stabf)/sizeof(char *))
    return stab[st];

  return "????";
}

/*
 * Make string version of chunk name.
 * Allow up to 16 simultaneous calls with distinct answers.
 */

public const char *c_nstr(chunk_t *c) {
  private int ix = -1;
  private char store[16][8];

  ix = (ix+1)&0xf;
  snprintf(&store[ix][0], 8, "c:%04hx", c->c_name);
  return &store[ix][0];
}
