#

/*
 * Copyright c. John Hallam <sw@j.hallam.dk> 2015.
 *
 * This program is free software licensed under the terms of the GNU General
 * Public License, either version 3 of the License, or (at your option) any
 * later version.  See http://www.gnu.org/licenses/gpl.txt for details.
 */

#include "general.h"

#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "clock.h"

/*
 * Get a value from the monotonic kernel clock and express in nanoseconds.
 */

public uint64_t monotonic_ns_clock() {
  uint64_t ret;
  struct timespec now;

  clock_gettime(CLOCK_MONOTONIC, &now);         /* Timestamp */
  ret = now.tv_sec;
  ret = ret*1000000000 + now.tv_nsec;
  return ret;
}

