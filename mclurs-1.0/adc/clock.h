#

/*
 * Copyright c. John Hallam <sw@j.hallam.dk> 2015.
 *
 * This program is free software licensed under the terms of the GNU General
 * Public License, either version 3 of the License, or (at your option) any
 * later version.  See http://www.gnu.org/licenses/gpl.txt for details.
 */

#ifndef _CLOCK_H
#define _CLOCK_H

#include "general.h"

#include <sys/capability.h>

export uint64_t monotonic_ns_clock();

#endif /* _CLOCK_H */
