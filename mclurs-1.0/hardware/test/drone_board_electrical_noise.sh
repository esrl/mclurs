#!/bin/zsh

dir="$(mktemp -d noise_test.XXX)"

echo "Files will written into '$dir'"

title="noise_test"

duration=20
supplies=("psu" "battery")
channels=4
delta=16              # gain values of 0, 0x10, 0x20...

gain -n x
for supply in $supplies; do
  for chan in `seq 1 $channels`; do
    for step in $(seq 0 16 255) 255; do
      hex=$(printf "%02x" "$step")
      file="${title}_${supply}_channel_${chan}_gain_0x${hex}"
      gain -n $step
      echo sudo =grab \| sox -c4 -r312.5k -ts16 - $dir/$file.wav remix 1 trim 0 ${duration} spectrogram -o $dir/$file.png
    done
  done
done
